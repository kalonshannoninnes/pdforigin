﻿namespace PDFOriginLib.Utils.Encoding
{
    public interface IEncoder
    {
        public string Encode(string stringToEncode);
        public string Decode(string encodedString);
    }
}