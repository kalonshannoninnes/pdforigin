﻿using System;

namespace PDFOriginLib.Utils.Encoding
{
    public class EncodingException : Exception
    {
        public EncodingException(string msg) : base(msg)
        {
            
        }
    }
}