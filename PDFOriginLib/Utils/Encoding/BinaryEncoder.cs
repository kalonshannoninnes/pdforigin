﻿using System.Linq;

namespace PDFOriginLib.Utils.Encoding
{
    public class BinaryEncoder : IEncoder
    {
        private const char Zero = '0';
        private const char One = '1';
        
        private readonly char _encodedZero;
        private readonly char _encodedOne;
        private readonly char _delimiter;

        public BinaryEncoder(char encodedZero, char encodedOne, char delimiter)
        {
            _encodedZero = encodedZero;
            _encodedOne = encodedOne;
            _delimiter = delimiter;
        }
        
        public string Encode(string stringToEncode)
        {
            if (!stringToEncode.All(c => c.Equals(Zero) || c.Equals(One)))
                throw new EncodingException("String contains non-binary characters");

            var addDelimiters = $"{_delimiter}{stringToEncode}{_delimiter}";
            var encoded = addDelimiters.Replace(Zero, _encodedZero).Replace(One, _encodedOne);
            
            return encoded;
        }

        public string Decode(string encodedString)
        {
            if (!encodedString.Contains(_delimiter)) 
                throw new EncodingException("Invalid encoded string");
            
            var start = encodedString.IndexOf(_delimiter) + 1;
            var end = encodedString.IndexOf(_delimiter, start);
            var substr = encodedString.Substring(start, end - start);
            
            if(!substr.All(c => c.Equals(_encodedOne) || c.Equals(_encodedZero))) 
                throw new EncodingException("Invalid encoded string");
            
            var removeDelimiters = substr.Replace(_delimiter.ToString(), string.Empty);
            var decoded = removeDelimiters.Replace(_encodedOne, One).Replace(_encodedZero, Zero);
            
            return decoded;
        }
    }
}