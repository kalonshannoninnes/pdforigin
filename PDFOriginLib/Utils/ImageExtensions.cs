﻿using System.Drawing;

namespace PDFOriginLib.Utils
{
    public static class ImageExtensions
    {
        /// <summary>
        ///     Apply the color fill operation to a bitmap
        /// </summary>
        /// <param name="bm">The bitmap to apply the color fill operation to</param>
        /// <param name="intensityThreshold">A value within the range 0-255 indicating how strict the fill should be</param>
        /// <param name="fillColor">The color to fill</param>
        public static void ColorFill(this Bitmap bm, byte intensityThreshold, Color fillColor)
        {
            for (var ii = 0; ii < bm.Width; ii++)
            {
                for (var jj = 0; jj < bm.Height; jj++)
                {
                    var pixel = bm.GetPixel(ii, jj);
                    var pixelMeetsThreshold = pixel.R < intensityThreshold
                                              && pixel.B < intensityThreshold
                                              && pixel.G < intensityThreshold;
                    
                    if(pixelMeetsThreshold)
                        bm.SetPixel(ii, jj, fillColor);
                }
            }
        }
    }
}