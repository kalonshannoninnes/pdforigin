﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using iText.Kernel.Font;

namespace PDFOriginLib.Pdf
{
    public interface IPdfService
    {
        public byte[] AddText(byte[] pdfBytes, int pageNum, string fontFilePath, string text, int x, int y);
        public byte[] AddWatermark(byte[] pdfBytes, int pageNum, string watermarkText);
        public List<string> SearchContentStream(byte[] pdfBytes, Regex regex);
        public int GetNumberOfPages(byte[] pdfBytes);
        public string GetStreamContent(byte[] pdfBytes, int pageNum);
        public byte[] SetStreamContent(byte[] pdfBytes, int pageNum, string replacementContent);
    }
}