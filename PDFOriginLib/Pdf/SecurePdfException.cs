﻿using System;

namespace PDFOriginLib.Pdf
{
    public class PDFOriginException : Exception
    {
        public PDFOriginException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}