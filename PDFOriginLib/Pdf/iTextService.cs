﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using iText.IO.Font;
using iText.Kernel;
using iText.Kernel.Colors;
using iText.Kernel.Font;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Kernel.Pdf.Colorspace;
using iText.Layout;
using iText.Layout.Element;
using IOException = iText.IO.IOException;
using iText.IO.Font.Constants;

namespace PDFOriginLib.Pdf
{
    // ReSharper disable once InconsistentNaming
    public class iTextService : IPdfService
    {
        /// <summary>
        ///     Add text to a specific page of a PDF document
        /// </summary>
        /// <param name="pdfBytes">a byte array representation of a PDF document</param>
        /// <param name="pageNum">the number of the page to add text to</param>
        /// <param name="font">a PdfFont to use when adding text</param>
        /// <param name="text">the text to add</param>
        /// <param name="x">The x coordinate to place the text</param>
        /// <param name="y">The y coordinate to place the text</param>
        /// <returns>a byte array representation of a PDF document with the text added to the specified page</returns>
        /// <exception cref="PDFOriginException">If there's a problem reading the font program,
        /// or there's a problem reading or writing to the PDF document</exception>
        public byte[] AddText(byte[] pdfBytes, int pageNum, string fontFilePath, string text, int x, int y)
        {
            try
            {
                var inputStream = new MemoryStream(pdfBytes);
                var outputStream = new MemoryStream();
                var doc = new PdfDocument(new PdfReader(inputStream), new PdfWriter(outputStream));
                var page = doc.GetPage(pageNum);
                var canvas = new PdfCanvas(page.GetFirstContentStream(), page.GetResources(), doc);
                var fontProgram = FontProgramFactory.CreateFont(fontFilePath, true);
                var font = PdfFontFactory.CreateFont(fontProgram, PdfEncodings.IDENTITY_H);
                
                canvas.BeginText()
                    .MoveText(x, y)
                    .SetFontAndSize(font, 12)
                    .ShowText(text)
                    .EndText();
            
                canvas.Release();
                doc.Close();
                return outputStream.ToArray();
            }
            catch (Exception e) when (e is PdfException or IOException)
            {
                throw new PDFOriginException(e.Message, e);
            }
        }

        /// <summary>
        ///     Add an invisible textual watermark to the background of a page in a PDF document
        /// </summary>
        /// <param name="pdfBytes">a byte array representation of a PDF document</param>
        /// <param name="pageNum">the page number</param>
        /// <param name="watermarkText">the text to use as a watermark</param>
        /// <returns>a byte array representation of a watermarked PDF document</returns>
        /// <exception cref="PDFOriginException">if there's an error reading or writing to the PDF file</exception>
        public byte[] AddWatermark(byte[] pdfBytes, int pageNum, string watermarkText)
        {
            try
            {
                const int textRbg = 253;
                const float textOpacity = 0.3f;
                const int tileWidth = 150;
                const int tileHeight = 32;
                
                var inputStream = new MemoryStream(pdfBytes);
                var outputStream = new MemoryStream();
                var doc = new PdfDocument(new PdfReader(inputStream), new PdfWriter(outputStream));
                
                var page = doc.GetPage(pageNum);
                var textColor = new DeviceRgb(textRbg, textRbg, textRbg);
                var font = PdfFontFactory.CreateFont(FontProgramFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                var paragraph = new Paragraph(watermarkText)
                    .SetFont(font)
                    .SetFontSize(tileHeight)
                    .SetFontColor(textColor, textOpacity);
            
                var tiling = new PdfPattern.Tiling(tileWidth, tileHeight);
                var _ = new Canvas(new PdfPatternCanvas(tiling, doc), tiling.GetBBox()).Add(paragraph);
                var pdfCanvas = new PdfCanvas(page.NewContentStreamBefore(), page.GetResources(), doc);

                pdfCanvas.SaveState()
                    .SetFillColor(new PatternColor(tiling))
                    .Rectangle(page.GetCropBox())
                    .Fill()
                    .RestoreState()
                    .Release(); // For some reason PdfCanvas doesn't implement IDisposable

                doc.Close();
                return outputStream.ToArray();
            }
            catch (Exception e) when (e is PdfException or IOException)
            {
                throw new PDFOriginException(e.Message, e);
            }
        }

        /// <summary>
        ///     Search the content streams of a document for the specified regex pattern
        /// </summary>
        /// <param name="pdfBytes">a byte array representation of a PDF document</param>
        /// <param name="regex">A regex pattern which should be searched for</param>
        /// <returns>A list of matches for the specified regex pattern</returns>
        /// <exception cref="PDFOriginException">if there's a problem reading the PDF document</exception>
        public List<string> SearchContentStream(byte[] pdfBytes, Regex regex)
        {
            try
            {
                var inputStream = new MemoryStream(pdfBytes);
                var doc = new PdfDocument(new PdfReader(inputStream));
                var totalPages = doc.GetNumberOfPages();
                var matches = new List<string>();
                
                for (var ii = 1; ii <= totalPages; ii++)
                {
                    var page = doc.GetPage(ii);
                    var content = Encoding.Default.GetString(page.GetFirstContentStream().GetBytes());
                    var match = regex.Match(content);
                    var syntax = match.Groups[1].Value.Replace("\n", " ");
                    
                    matches.Add(syntax);
                }

                doc.Close();
                return matches;
            }
            catch (Exception e) when (e is PdfException or IOException)
            {
                throw new PDFOriginException(e.Message, e);
            }
        }
        
        /// <summary>
        ///     Get the total number of pages in a PDF document
        /// </summary>
        /// <param name="pdfBytes">a byte array representation of a PDF document</param>
        /// <returns>the total number of pages in the specified document</returns>
        /// <exception cref="PDFOriginException">if there's a problem reading the pdf document</exception>
        public int GetNumberOfPages(byte[] pdfBytes)
        {
            try
            {
                var inputStream = new MemoryStream(pdfBytes);
                var doc = new PdfDocument(new PdfReader(inputStream));
                var totalPages = doc.GetNumberOfPages();
                
                doc.Close();
                return totalPages;
            }
            catch (Exception e) when (e is PdfException or IOException)
            {
                throw new PDFOriginException(e.Message, e);
            }
        }

        /// <summary>
        ///     Get the content of the stream for the specified page
        /// </summary>
        /// <param name="pdfBytes">a byte array representation of a PDF document</param>
        /// <param name="pageNum">the page number</param>
        /// <returns>the decoded content of the specified page's content stream</returns>
        /// <exception cref="PDFOriginException">if there's a problem reading the pdf document</exception>
        public string GetStreamContent(byte[] pdfBytes, int pageNum)
        {
            try
            {
                var inputStream = new MemoryStream(pdfBytes);
                var doc = new PdfDocument(new PdfReader(inputStream));
                var stream = doc.GetPage(pageNum).GetFirstContentStream();
                var content = Encoding.Default.GetString(stream.GetBytes());

                doc.Close();
                return content;
            }
            catch (Exception e) when (e is PdfException or IOException)
            {
                throw new PDFOriginException(e.Message, e);
            }
        }

        /// <summary>
        ///     Set the content of the stream for the specified page
        /// </summary>
        /// <param name="pdfBytes">a byte array representation of a PDF document</param>
        /// <param name="pageNum">the page number</param>
        /// <param name="replacementContent">the unencoded content to replace with</param>
        /// <returns>a byte array representation of a PDF document with the modified page content</returns>
        /// <exception cref="PDFOriginException">if there's an error reading or writing to the PDF document</exception>
        public byte[] SetStreamContent(byte[] pdfBytes, int pageNum, string replacementContent)
        {
            try
            {
                var inputStream = new MemoryStream(pdfBytes);
                var outputStream = new MemoryStream();
                var doc = new PdfDocument(new PdfReader(inputStream), new PdfWriter(outputStream));
                var stream = doc.GetPage(pageNum).GetFirstContentStream();
                
                stream.SetData(Encoding.Default.GetBytes(replacementContent));

                doc.Close();
                return outputStream.ToArray();
            }
            catch (Exception e) when (e is PdfException or IOException)
            {
                throw new PDFOriginException(e.Message, e);
            }
        }
    }
}