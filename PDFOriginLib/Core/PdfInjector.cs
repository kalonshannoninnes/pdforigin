﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using PDFOriginLib.Pdf;
using PDFOriginLib.Utils.Encoding;
using iTextIOException = iText.IO.IOException;
using StandardIOException = System.IO.IOException;

namespace PDFOriginLib.Core
{
    public class PdfInjector : IInjector
    {
        private const string NegativeId = "IDs cannot be negative";
        
        private const string FontPath = @"Resources/lucida_sans_unicode.ttf";

        private readonly IEncoder _encoder;
        private readonly IPdfService _pdfService;

        public PdfInjector(IEncoder encoder, IPdfService pdfService)
        {
            _encoder = encoder;
            _pdfService = pdfService;
        }

        /// <summary>
        ///     Insert a unique identifier throughout the specified PDF document
        ///     This identifier will be invisible to the naked eye, but persist
        ///     through attempts to copy/paste text.
        /// </summary>
        /// <param name="pdfBytes">A byte array representation of a PDF file</param>
        /// <param name="id">The unique ID to insert into the PDF file</param>
        /// <param name="encoder">an instance of IEncoder to encode IDs</param>
        /// <returns>A byte array representation of a fingerprinted PDF file</returns>
        /// <exception cref="ArgumentException">if id is negative</exception>
        /// <exception cref="PDFOriginException">if there's an error reading from, or writing to, the pdf file</exception>
        public byte[] InjectHiddenCharacters(byte[] pdfBytes, long id)
        {
            if (id < 0) throw new ArgumentException(NegativeId);

            var binaryId = Convert.ToString(id, 2);
            var encodedId = _encoder.Encode(binaryId);
            var templatePdf = InsertEmbedTemplates(pdfBytes, encodedId);
            var embeddingSyntaxes = GetEmbeddingSyntaxes(templatePdf);
            var outputFileBytes = Fingerprint(templatePdf, embeddingSyntaxes);

            return outputFileBytes;
        }
        
        /// <summary>
        ///     Apply a background watermark throughout the specified PDF document
        ///     This watermark will be almost invisible to the naked eye, but revealable
        ///     via the FingerprintExtractor
        /// </summary>
        /// <param name="pdfBytes">a byte array representation of a PDF file</param>
        /// <param name="id">The unique ID to insert into the PDF file</param>
        /// <returns>a byte array representation of a fingerprinted PDF file</returns>
        /// <exception cref="ArgumentException">if id is negative</exception>
        /// <exception cref="PDFOriginException">if there's an error reading from, or writing to, the pdf file</exception>
        public byte[] InjectWatermark(byte[] pdfBytes, long id)
        {
            if (id < 0) throw new ArgumentException(NegativeId);

            var totalPages = _pdfService.GetNumberOfPages(pdfBytes);
            var editedPdf = pdfBytes;

            for (var ii = 1; ii <= totalPages; ii++)
            {
                editedPdf = _pdfService.AddWatermark(editedPdf, ii, id.ToString());
            }

            return editedPdf;
        }

        // Insert a hidden ID at the bottom of each page in the PDF
        private byte[] InsertEmbedTemplates(byte[] pdfBytes, string id)
        {
            var totalPages = _pdfService.GetNumberOfPages(pdfBytes);
            var editedPdf = pdfBytes;

            for (var ii = 1; ii <= totalPages; ii++)
            {
                editedPdf = _pdfService.AddText(editedPdf, ii, FontPath, id, 0, 0);
            }

            return editedPdf;
        }
        
        // Extract the PDF-encoded ID and Font setting from each page
        private List<string> GetEmbeddingSyntaxes(byte[] pdfBytes)
        {
            const string embedSyntaxTemplate = @"BT\n\d* \d* Td\n(\/F\d \d+ Tf\n<.*>Tj\nET)";
            
            return _pdfService.SearchContentStream(pdfBytes, new Regex(embedSyntaxTemplate));
        }
        
        // For each page, apply its specifically encoded ID using the specific font settings
        private byte[] Fingerprint(byte[] pdfBytes, IReadOnlyList<string> fingerprintSyntax)
        {
            var totalPages = _pdfService.GetNumberOfPages(pdfBytes);
            var editedPdf = pdfBytes;

            for (var ii = 1; ii <= totalPages; ii++)
            {
                var matchingEmbed = fingerprintSyntax[ii - 1];
                var content = _pdfService.GetStreamContent(editedPdf, ii);
                var replacedText = content.Replace("ET", matchingEmbed);
                editedPdf = _pdfService.SetStreamContent(editedPdf, ii, replacedText);
            }

            return editedPdf;
        }
    }
}