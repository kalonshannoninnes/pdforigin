﻿using System;

namespace PDFOriginLib.Core
{
    public class FingerprintNotFoundException : Exception
    {
        public FingerprintNotFoundException(string message) : base(message)
        {
        }

        public FingerprintNotFoundException()
        {
            
        }
    }
}