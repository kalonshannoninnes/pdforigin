﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using PDFOriginLib.Utils;
using PDFOriginLib.Utils.Encoding;

namespace PDFOriginLib.Core
{
    public class PdfExtractor : IExtractor
    {
        private readonly IEncoder _encoder;
        
        private const string IdNotFound = "No ID found in text";

        public PdfExtractor(IEncoder encoder)
        {
            _encoder = encoder;
        }
        
        /// <summary>
        ///     Extract a unique fingerprint from some the supplied text
        /// </summary>
        /// <param name="text"></param>
        /// <returns>a unique ID fingerprint</returns>
        /// <exception cref="KeyNotFoundException">If no ID is found in the specified text</exception>
        public string ExtractHiddenCharacters(string text)
        {
            try
            {
                var decodedBinary = _encoder.Decode(text);
                return Convert.ToInt64(decodedBinary, 2).ToString();
            }
            catch (EncodingException e)
            {
                throw new FingerprintNotFoundException("No valid fingerprint found in text");
            }
        }

        /// <summary>
        ///     Reveal the hidden watermark within an image
        /// </summary>
        /// <param name="imgBytes">a byte array representation of an image</param>
        /// <returns>a byte array representation of an image with the watermark revealed</returns>
        public byte[] RevealWatermark(byte[] imgBytes)
        {   
            // TODO Look into OCR for the image
            using var inputStream = new MemoryStream(imgBytes);
            using var outputStream = new MemoryStream();
            var bm = new Bitmap(inputStream); // Change to be compatible with Linux (Bitmap is not)
            
            bm.ColorFill(255, Color.Black);
            bm.Save(outputStream, ImageFormat.Png);
            
            return outputStream.ToArray();
        }
    }
}