﻿using PDFOriginLib.Utils.Encoding;

namespace PDFOriginLib.Core
{
    public interface IInjector
    {
        public byte[] InjectHiddenCharacters(byte[] pdfBytes, long id);
        public byte[] InjectWatermark(byte[] pdfBytes, long id);
    }
}