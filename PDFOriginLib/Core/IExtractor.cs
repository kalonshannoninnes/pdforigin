﻿using PDFOriginLib.Utils.Encoding;

namespace PDFOriginLib.Core
{
    public interface IExtractor
    {
        public string ExtractHiddenCharacters(string text);
        public byte[] RevealWatermark(byte[] imgBytes);
    }
}