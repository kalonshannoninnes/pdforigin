using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Threading;
using MessageBox.Avalonia;
using ReactiveUI;
using PDFOriginAvalonia.Models;
using PDFOriginAvalonia.Services.Email;

namespace PDFOriginAvalonia.ViewModels
{
    public class EmailViewModel : ViewModelBase
    {
        // Injected Dependencies
        private readonly Window _parent;
        private readonly IEmailService _emailService;
        private readonly string _homeDir;
        
        // User Input Dependencies
        public Interaction<EmailConfigViewModel, EmailConfig> ShowConfigDialog { get; }
        private EmailConfigViewModel CurrentConfigViewModel { get; set; }
        private EmailConfig Config { get; set; }
        private Dictionary<string, string> IdFileMap { get; set; }

        public EmailViewModel(Window parent, IEmailService emailService, string homeDir)
        {
            _parent = parent;
            _emailService = emailService;
            _homeDir = homeDir;
            
            IdFileMap = new Dictionary<string, string>();
            CurrentConfigViewModel = new EmailConfigViewModel();
            Config = new EmailConfig();
            ShowConfigDialog = new Interaction<EmailConfigViewModel, EmailConfig>();
            Body = "Dear student,\n\n" +
                   "Please find attached assessment document.\n" +
                   "Please DO NOT share this file with other people. " +
                   "It has been fingerprinted with your ID. " +
                   "You can make your own private copies on your own devices or accounts, " +
                   "but nobody else should be given access to them.";

            var canGenerateMappings = this.WhenAnyValue(x => x.EmailTemplate, 
                emailTemplate => !string.IsNullOrEmpty(emailTemplate));

            var canSendEmail = this.WhenAnyValue(x => x.IdEmailMap, x => x.From,
                (emailMap, from) => !string.IsNullOrEmpty(emailMap) && !string.IsNullOrEmpty(from));

            SelectDirectoryCommand = ReactiveCommand.Create(SelectDirectory);
            GenerateMappingsCommand = ReactiveCommand.Create(GenerateMappings, canGenerateMappings);
            ShowConfigCommand = ReactiveCommand.Create(ShowConfig);
            ClearCommand = ReactiveCommand.Create(Clear);
            SendEmailCommand = ReactiveCommand.Create(SendEmail, canSendEmail);
        }

        // UI Bindings
        private string _idFilePath;
        private string IdFilePath
        {
            get => _idFilePath;
            set => this.RaiseAndSetIfChanged(ref _idFilePath, value);
        }

        private string _emailTemplate;
        private string EmailTemplate
        {
            get => _emailTemplate;
            set => this.RaiseAndSetIfChanged(ref _emailTemplate, value);
        }

        private string _idEmailMap;
        private string IdEmailMap
        {
            get => _idEmailMap;
            set => this.RaiseAndSetIfChanged(ref _idEmailMap, value);
        }

        private string _displayName;
        private string DisplayName
        {
            get => _displayName;
            set => this.RaiseAndSetIfChanged(ref _displayName, value);
        }

        private string _from;
        private string From
        {
            get => _from;
            set => this.RaiseAndSetIfChanged(ref _from, value);
        }

        private string _subject;
        private string Subject
        {
            get => _subject;
            set => this.RaiseAndSetIfChanged(ref _subject, value);
        }

        private string _body;
        private string Body
        {
            get => _body;
            set => this.RaiseAndSetIfChanged(ref _body, value);
        }
        
        private double _progress;
        private double Progress
        {
            get => _progress;
            set => this.RaiseAndSetIfChanged(ref _progress, value);
        }
        
        // UI Command Bindings
        private ReactiveCommand<Unit, Task> SelectDirectoryCommand { get; }
        private async Task SelectDirectory()
        {
            var folderDialog = new OpenFolderDialog() { Directory = _homeDir};
            IdFilePath = await folderDialog.ShowAsync(_parent);
        }

        private ReactiveCommand<Unit, Unit> GenerateMappingsCommand { get; }
        private void GenerateMappings()
        {
            var allPdfFiles = Directory.GetFiles(IdFilePath).Where(filename => filename.EndsWith(".pdf"));
            var ids = allPdfFiles.Select(path => Path.GetFileName(path).Split('_')[0]).ToList();
            IdFileMap = ids.ToDictionary(id => id, id => Directory.GetFiles(IdFilePath).First(file => file.Contains(id)));
            IdEmailMap = ids.Aggregate("", (acc, id) => acc + $"{id} {EmailTemplate.Replace("%i", id)}\n").Trim();
        }

        private ReactiveCommand<Unit, Task> ShowConfigCommand { get; }
        private async Task ShowConfig()
        {
            Config = await ShowConfigDialog.Handle(CurrentConfigViewModel);
        }

        private ReactiveCommand<Unit, Unit> ClearCommand { get; }
        private void Clear()
        {
            IdFilePath = "";
            EmailTemplate = "";
            DisplayName = "";
            From = "";
            Subject = "";
            Body = "";
        }
        
        // TODO Implement button disabling like encoding tab
        private ReactiveCommand<Unit, Task> SendEmailCommand { get; }
        private async Task SendEmail()
        {
            Progress = 0;
            var idEmailMap = IdEmailMap.Split(Environment.NewLine);
            var progressStep = 100.0 / idEmailMap.Length;
            
            var emailTasks = idEmailMap.Select(idEmail =>
            {
                return Task.Run(async () =>
                {
                    var splitMap = idEmail.Split(" ");
                    var file = IdFileMap[splitMap[0]];
                    var to = splitMap[1];
            
                    var msg = await _emailService.GenerateMailMessage(From, DisplayName, to, Subject, Body, file);
                    await _emailService.Send(Config.Server, Config.Port, msg);
                    Dispatcher.UIThread.Post(() => Progress += progressStep);
                    Dispatcher.UIThread.Post(() => RemoveFromIdEmailMap(splitMap[0]));
                });
            });

            try
            {
                await Task.WhenAll(emailTasks);
            }
            catch (Exception e)
            {
                var errorMsg = MessageBoxManager.GetMessageBoxStandardWindow("Error", e.ToString());
                await errorMsg.ShowDialog(_parent);
            }

            Clear();
        }

        private void RemoveFromIdEmailMap(string id)
        {
            var newIdEmailMap = IdEmailMap.Split("\n").Where(line => !line.StartsWith(id));
            IdEmailMap = string.Join("\n", newIdEmailMap);
        }
    }
}