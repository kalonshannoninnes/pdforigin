using System;
using System.IO;
using System.Reactive;
using System.Threading.Tasks;
using Avalonia.Controls;
using MessageBox.Avalonia;
using ReactiveUI;
using PDFOriginLib.Utils.Encoding;
using PDFOriginAvalonia.Services;
using PDFOriginAvalonia.Views.DecodeTabView;
using Bitmap = Avalonia.Media.Imaging.Bitmap;

namespace PDFOriginAvalonia.ViewModels
{
    // TODO Change exception handling in here to not catch exceptions from core
    public class DecodeViewModel : ViewModelBase
    {
        private readonly Window _parent;
        private readonly IEncodingService _encodingService;
        private readonly string _homeDir;

        public DecodeViewModel(Window parent, IEncodingService encodingService, string homeDir)
        {
            _parent = parent;
            _encodingService = encodingService;
            _homeDir = homeDir;
            EncodedText = string.Empty;

            SelectEncodedImageCommand = ReactiveCommand.Create(SelectEncodedImage);
            
            var canDecodeImage = this.WhenAnyValue(x => x.ImagePath, text => !string.IsNullOrEmpty(text));
            DecodeImageWatermarkCommand = ReactiveCommand.Create(DecodeImageWatermark, canDecodeImage);

            var canDecodeText = this.WhenAnyValue(x => x.EncodedText, text => !string.IsNullOrEmpty(text));
            DecodeTextCommand = ReactiveCommand.Create(DecodeText, canDecodeText);
        }
        
        // UI Bindings
        private string _encodedText;
        private string EncodedText
        {
            get => _encodedText;
            set => this.RaiseAndSetIfChanged(ref _encodedText, value);
        }

        private string _imagePath;
        private string ImagePath
        {
            get => _imagePath;
            set => this.RaiseAndSetIfChanged(ref _imagePath, value);
        }

        private Bitmap _revealedWatermark;
        private Bitmap RevealedWatermark
        {
            get => _revealedWatermark;
            set => this.RaiseAndSetIfChanged(ref _revealedWatermark, value);
        }
        
        // UI Command Bindings
        private ReactiveCommand<Unit, Task> SelectEncodedImageCommand { get; }
        private async Task SelectEncodedImage()
        {
            var dialog = new OpenFileDialog() { Directory = _homeDir };
            var filePaths = await dialog.ShowAsync(_parent);
            if (filePaths.Length > 0) ImagePath = filePaths[0];
        }

        private ReactiveCommand<Unit, Task> DecodeImageWatermarkCommand { get; }
        private async Task DecodeImageWatermark()
        {
            try
            {
                var imageBytes = await File.ReadAllBytesAsync(ImagePath);
                var revealedImage = _encodingService.DecodeImage(imageBytes);
                await using var ms = new MemoryStream(revealedImage);
                RevealedWatermark = new Bitmap(ms);

                var wmDialog = new RevealedWatermarkDialog
                {
                    WindowStartupLocation = WindowStartupLocation.CenterOwner,
                    DataContext = _parent.DataContext
                };
                await wmDialog.ShowDialog(_parent);
            }
            catch (Exception e)
            {
                var errorMsg = MessageBoxManager.GetMessageBoxStandardWindow("Error", e.ToString());
                await errorMsg.ShowDialog(_parent);
            }
        }

        private ReactiveCommand<Unit, Task> DecodeTextCommand { get; }
        private async Task DecodeText()
        {
            try
            {
                var decodedId = _encodingService.DecodeText(EncodedText);
                var msg = $"Student ID: {decodedId}";

                var decodedIdMsgBox = MessageBoxManager.GetMessageBoxStandardWindow("Decoded Student ID", msg);
                await decodedIdMsgBox.ShowDialog(_parent);
            }
            catch (EncodingException e)
            {
                var errorMsgBox = MessageBoxManager.GetMessageBoxStandardWindow("Decoding Error", e.Message);
                await errorMsgBox.ShowDialog(_parent);
            }
            catch (Exception e)
            {
                var errorMsgBox = MessageBoxManager.GetMessageBoxStandardWindow("Error", e.ToString());
                await errorMsgBox.ShowDialog(_parent);
            }
        }
    }
}