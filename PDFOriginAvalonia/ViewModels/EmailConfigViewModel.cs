using System.Reactive;
using ReactiveUI;
using PDFOriginAvalonia.Models;

namespace PDFOriginAvalonia.ViewModels
{
    public class EmailConfigViewModel : ViewModelBase
    {
        public EmailConfigViewModel()
        {
            CurrentConfig = new EmailConfig();
            UpdatedConfig = new EmailConfig();
            
            CloseCommand = ReactiveCommand.Create(() =>
            {
                UpdatedConfig = CurrentConfig;
                return CurrentConfig;
            });
            
            ApplyAndCloseCommand = ReactiveCommand.Create(() =>
            {
                CurrentConfig = UpdatedConfig;
                return CurrentConfig;
            });
        }
        
        // UI Bindings
        public EmailConfig UpdatedConfig { get; set; }
        public EmailConfig CurrentConfig { get; set; }
        
        // UI Command Bindings
        public ReactiveCommand<Unit, EmailConfig> ApplyAndCloseCommand { get; }
        public ReactiveCommand<Unit, EmailConfig> CloseCommand { get; }
    }
}