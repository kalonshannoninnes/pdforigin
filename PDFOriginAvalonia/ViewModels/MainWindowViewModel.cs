﻿using System;
using System.Threading.Tasks;
using Avalonia.Controls;
using PDFOriginAvalonia.Views.EmailTabView.EmailConfigViews;

namespace PDFOriginAvalonia.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public EncodeViewModel Encode { get; }
        public EmailViewModel Email { get; }
        public DecodeViewModel Decode { get; }

        public MainWindowViewModel(
            EncodeViewModel encodeViewModel, 
            EmailViewModel emailViewModel, 
            DecodeViewModel decodeViewModel)
        {
            Encode = encodeViewModel;
            Decode = decodeViewModel;
            Email = emailViewModel;
        }
    }
}