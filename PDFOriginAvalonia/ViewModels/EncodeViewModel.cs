using System;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Threading;
using MessageBox.Avalonia;
using ReactiveUI;
using PDFOriginLib.Utils.Encoding;
using PDFOriginAvalonia.Services;

namespace PDFOriginAvalonia.ViewModels
{
    public class EncodeViewModel : ViewModelBase
    {
        // Injected Dependencies
        private readonly Window _parent;
        private readonly IEncodingService _encodeService;
        private readonly IFileIoService _fileIoService;
        private readonly string _homeDir;

        public EncodeViewModel(Window parent, IEncodingService encodeService, IFileIoService fileIoService, string homeDir)
        {
            _parent = parent;
            _encodeService = encodeService;
            _fileIoService = fileIoService;
            _homeDir = homeDir;
            
            IsEncoding = false;

            var canEncode = this.WhenAnyValue(x => x.TemplateFilePath, x => x.OutputDirectoryPath, x => x.IsEncoding,
                (file, output, isEncoding) => 
                    !string.IsNullOrEmpty(file) && 
                    !string.IsNullOrEmpty(output) && 
                    !isEncoding);

            var canClear = this.WhenAnyValue(x => x.IsEncoding, buttonsEnabled => !buttonsEnabled);

            SelectTemplateFileCommand = ReactiveCommand.Create(SelectTemplateFile);
            SelectOutputDirectoryCommand = ReactiveCommand.Create(SelectOutputDirectory);
            SelectIdFileCommand = ReactiveCommand.Create(SelectIdFile);
            EncodeCommand = ReactiveCommand.Create(Encode, canEncode);
            ClearCommand = ReactiveCommand.Create(Clear, canClear);
            
            Clear();
        }

        // UI Bindings
        private string _templateFilePath;
        private string TemplateFilePath
        {
            get => _templateFilePath;
            set => this.RaiseAndSetIfChanged(ref _templateFilePath, value);
        }

        private string _outputDirPath;
        private string OutputDirectoryPath
        {
            get => _outputDirPath;
            set => this.RaiseAndSetIfChanged(ref _outputDirPath, value);
        }

        private string _idFilePath;
        private string IdFilePath
        {
            get => _idFilePath;
            set => this.RaiseAndSetIfChanged(ref _idFilePath, value);
        }

        private string _ids;
        private string Ids
        {
            get => _ids;
            set => this.RaiseAndSetIfChanged(ref _ids, value);
        }
        
        private double _progress;
        private double Progress
        {
            get => _progress;
            set => this.RaiseAndSetIfChanged(ref _progress, value);
        }
        
        private bool _isEncoding;
        private bool IsEncoding
        {
            get => _isEncoding;
            set => this.RaiseAndSetIfChanged(ref _isEncoding, value);
        }

        // UI Command Bindings
        private ReactiveCommand<Unit, Task> SelectTemplateFileCommand { get; }
        private async Task SelectTemplateFile()
        {
            var filePaths = await SelectFile();
            if (filePaths.Length > 0) TemplateFilePath = filePaths[0];
        }

        private ReactiveCommand<Unit, Task> SelectOutputDirectoryCommand { get; }
        private async Task SelectOutputDirectory()
        {
            var dirPath = await SelectDirectory();
            if (!string.IsNullOrEmpty(dirPath)) OutputDirectoryPath = dirPath;
        }

        private ReactiveCommand<Unit, Task> SelectIdFileCommand { get; }
        private async Task SelectIdFile()
        {
            var filePaths = await SelectFile();
            if (filePaths.Length > 0)
            {
                IdFilePath = filePaths[0];
                await LoadIds(IdFilePath);
            }
        }

        private ReactiveCommand<Unit, Task> EncodeCommand { get; }
        private async Task Encode()
        {
            IsEncoding = true;
            Progress = 0;

            try
            {
                var templatePdf = await File.ReadAllBytesAsync(TemplateFilePath);
                var ids = Ids.Split();
                var progStep = 100.0 / Math.Max(ids.Length, 1);
                var encodingTasks = ids.Select(id => CreateEncodingTask(id, templatePdf, progStep));

                await Task.WhenAll(encodingTasks);
                Clear();
            }
            catch (EncodingException e) // TODO Don't catch this exception type here
            {
                var errorMsgBox = MessageBoxManager.GetMessageBoxStandardWindow("Encoding Error", e.Message);
                await errorMsgBox.ShowDialog(_parent);
            }
            catch (Exception e) when (e is FileNotFoundException or DirectoryNotFoundException)
            {
                var dialog = MessageBoxManager.GetMessageBoxStandardWindow("IO Error", e.Message);
                await dialog.ShowDialog(_parent);
            }
            catch (Exception e)
            {
                var dialog = MessageBoxManager.GetMessageBoxStandardWindow("General Error", e.ToString());
                await dialog.ShowDialog(_parent);
            }

            IsEncoding = false;
        }

        private ReactiveCommand<Unit, Unit> ClearCommand { get; }
        private void Clear()
        {
            TemplateFilePath = "";
            OutputDirectoryPath = "";
            IdFilePath = "";
        }

        // Helpers
        private async Task<string[]> SelectFile()
        {
            var fileDialog = new OpenFileDialog() { Directory = _homeDir};
            return await fileDialog.ShowAsync(_parent);
        }

        private async Task<string> SelectDirectory()
        {
            var dirDialog = new OpenFolderDialog(){ Directory = _homeDir };
            return await dirDialog.ShowAsync(_parent);
        }

        private async Task LoadIds(string filePath)
        {
            var ids = await File.ReadAllLinesAsync(filePath);
            Ids = string.Join(' ', ids);
        }

        private void UpdateProgress(string id, double progStep)
        {
            Progress += progStep;
            var newIds = Ids.Split(null).ToList();
            newIds.Remove(id);
            Ids = string.Join(' ', newIds);
        }

        private Task CreateEncodingTask(string id, byte[] templatePdf, double progStep)
        {
            return Task.Run(async () =>
            {
                var outputFileName = await _fileIoService.GetOutputFileName(TemplateFilePath, OutputDirectoryPath, id);
                var encodedBytes = _encodeService.Encode(templatePdf, id);
                await File.WriteAllBytesAsync(outputFileName, encodedBytes);
                Dispatcher.UIThread.Post(() => UpdateProgress(id, progStep)); // Dispatching required to ensure progress is updated synchronously
            });
        }
    }
}