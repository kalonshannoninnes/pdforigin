using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace PDFOriginAvalonia.Views.EmailTabView
{
    public class EmailMessageView : UserControl
    {
        public EmailMessageView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}