using System;
using Avalonia;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using ReactiveUI;
using PDFOriginAvalonia.ViewModels;

namespace PDFOriginAvalonia.Views.EmailTabView.EmailConfigViews
{
    public class EmailConfigView : ReactiveWindow<EmailConfigViewModel>
    {
        public EmailConfigView()
        {
            InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif
            this.WhenActivated(d => d(ViewModel!.ApplyAndCloseCommand.Subscribe(Close)));
            this.WhenActivated(d => d(ViewModel!.CloseCommand.Subscribe(Close)));
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
        
        
    }
}