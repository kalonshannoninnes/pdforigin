using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using PDFOriginAvalonia.ViewModels;

namespace PDFOriginAvalonia.Views.EmailTabView
{
    public class EmailView : UserControl
    {
        public EmailView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}