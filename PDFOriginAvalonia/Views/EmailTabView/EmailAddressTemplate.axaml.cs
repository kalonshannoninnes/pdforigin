using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace PDFOriginAvalonia.Views.EmailTabView
{
    public class EmailAddressTemplate : UserControl
    {
        public EmailAddressTemplate()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}