using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace PDFOriginAvalonia.Views.EncodeTabView
{
    public class OutputFileView : UserControl
    {
        public OutputFileView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}