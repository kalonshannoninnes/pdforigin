using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace PDFOriginAvalonia.Views.EncodeTabView
{
    public class ProgressBarView : UserControl
    {
        public ProgressBarView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}