using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using PDFOriginAvalonia.ViewModels;

namespace PDFOriginAvalonia.Views.EncodeTabView
{
    public class EncodeView : UserControl
    {
        public EncodeView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}