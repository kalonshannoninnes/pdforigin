using System;
using System.Net.Mail;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using ReactiveUI;
using PDFOriginLib.Core;
using PDFOriginLib.Pdf;
using PDFOriginLib.Utils.Encoding;
using PDFOriginAvalonia.Models;
using PDFOriginAvalonia.Services;
using PDFOriginAvalonia.Services.Email;
using PDFOriginAvalonia.ViewModels;
using PDFOriginAvalonia.Views.EmailTabView.EmailConfigViews;

namespace PDFOriginAvalonia.Views
{
    public partial class MainWindow : ReactiveWindow<MainWindowViewModel>
    {
        public MainWindow()
        {
            InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif
            const char delimiter = '​';
            const char zero = '‌';
            const char one = '‍';

            // TODO Fix difference between service and non-service (Service Utils?)

            var homeDir = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            
            var encoding = new BinaryEncoder(zero, one, delimiter);
            var extractor = new PdfExtractor(encoding);
            var injector = new PdfInjector(encoding, new iTextService());
            
            var encodingService = new PdfEncodingService(injector, extractor);
            var emailService = new EmailService();
            var fileService = new FileIoService();
            
            var encodeViewModel = new EncodeViewModel(this, encodingService, fileService, homeDir);
            var emailViewModel = new EmailViewModel(this, emailService, homeDir);
            var decodeViewModel = new DecodeViewModel(this, encodingService, homeDir);
            
            DataContext = new MainWindowViewModel(encodeViewModel, emailViewModel, decodeViewModel);
            
            this.WhenActivated(d => d(ViewModel!.Email.ShowConfigDialog.RegisterHandler(HandleShowConfigDialog)));
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        private async Task HandleShowConfigDialog(InteractionContext<EmailConfigViewModel, EmailConfig> interaction)
        {
            var dialog = new EmailConfigView();
            dialog.DataContext = interaction.Input;

            var result = await dialog.ShowDialog<EmailConfig?>(this) ?? new EmailConfig();
            interaction.SetOutput(result);
        }
    }
}