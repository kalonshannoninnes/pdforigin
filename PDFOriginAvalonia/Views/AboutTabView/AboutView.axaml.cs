using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace PDFOriginAvalonia.Views.AboutTabView
{
    public class AboutView : UserControl
    {
        public AboutView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}