using System.Collections.Generic;
using System.Threading.Tasks;

namespace PDFOriginAvalonia.Services.Email
{
    public interface ITemplateResolver
    {
        string Resolve(string input, string template);
    }
}