using System.Net.Mail;
using System.Threading.Tasks;

namespace PDFOriginAvalonia.Services.Email
{
    public class SmtpEmailClient : IEmailClient
    {
        private readonly SmtpClient _client;

        private string _host;
        string IEmailClient.Host
        {
            get => _host;
            set
            {
                _client.Host = value;
                _host = value;
            }
        }
        
        private int _port;
        int IEmailClient.Port
        {
            get => _port;
            set
            {
                _client.Port = value;
                _port = value;
            }
        }

        public SmtpEmailClient(SmtpClient client)
        {
            _client = client;
        }

        public async Task SendMailAsync(MailMessage message) => await _client.SendMailAsync(message);
    }
}