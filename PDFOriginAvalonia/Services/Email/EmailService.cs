using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;

namespace PDFOriginAvalonia.Services.Email
{
    public class EmailService : IEmailService
    {
        public Task<MailMessage> GenerateMailMessage(string from, string displayName, string to, 
                                                    string subject, string body, string fileAttachment)
        {
            return Task.FromResult(new MailMessage
            {
                From = new MailAddress(from, displayName),
                To = {new MailAddress(to)},
                Subject = subject,
                Body = body,
                Attachments = { new Attachment(fileAttachment) }
            });
        }

        public async Task Send(string server, int port, MailMessage email)
        {
            using var client = new SmtpClient();
            // client.Host = server;
            // client.Port = port;

            client.Host = "smtp.mailtrap.io";
            client.Port = 2525;
            client.Credentials = new NetworkCredential("e631331670ded1", "52c96e8f5857b1");
            client.EnableSsl = true;
            
            await client.SendMailAsync(email);
        }
    }
}