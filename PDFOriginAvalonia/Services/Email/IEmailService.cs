using System.Net.Mail;
using System.Threading.Tasks;

namespace PDFOriginAvalonia.Services.Email
{
    public interface IEmailService
    {
        Task<MailMessage> GenerateMailMessage(string from, string displayName, string to, 
                                        string subject, string body, string fileAttachment);
        Task Send(string server, int port, MailMessage email);
    }
}