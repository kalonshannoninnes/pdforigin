using System.Net.Mail;
using System.Threading.Tasks;

namespace PDFOriginAvalonia.Services.Email
{
    public interface IEmailClient
    {
        public string Host { get; set; }
        public int Port { get; set; }
        Task SendMailAsync(MailMessage message);
    }
}