namespace PDFOriginAvalonia.Services
{
    public interface IEncodingService
    {
        public byte[] Encode(byte[] pdfBytes, string id);
        public string DecodeText(string text);
        public byte[] DecodeImage(byte[] imageBytes);
    }
}