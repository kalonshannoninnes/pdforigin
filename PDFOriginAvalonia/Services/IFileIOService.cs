using System.Threading.Tasks;

namespace PDFOriginAvalonia.Services
{
    public interface IFileIoService
    {
        public Task<string> GetOutputFileName(string inputFilePath, string outputDir, string fileNamePrefix);
    }
}