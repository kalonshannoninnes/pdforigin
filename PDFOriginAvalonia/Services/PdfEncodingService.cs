using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using PDFOriginLib.Core;
using PDFOriginLib.Pdf;
using PDFOriginLib.Utils.Encoding;

namespace PDFOriginAvalonia.Services
{
    public class PdfEncodingService : IEncodingService
    {
        private readonly IInjector _injector;
        private readonly IExtractor _extractor;

        public PdfEncodingService(IInjector injector, IExtractor extractor)
        {
            _injector = injector;
            _extractor = extractor;
        }

        public byte[] Encode(byte[] pdfBytes, string id)
        {
            // TODO This check might be happening multiple times, here and in core
            if (!long.TryParse(id.Trim(), out var idLong)) throw new FormatException($"Error parsing {id}: bad format");
            
            var fingerprintedPdf = _injector.InjectHiddenCharacters(pdfBytes, idLong);
            fingerprintedPdf = _injector.InjectWatermark(fingerprintedPdf, idLong);

            return fingerprintedPdf;
        }

        public string DecodeText(string encodedText)
        {
            return _extractor.ExtractHiddenCharacters(encodedText);
        }

        public byte[] DecodeImage(byte[] imageBytes)
        {
            return _extractor.RevealWatermark(imageBytes);
        }
    }
}