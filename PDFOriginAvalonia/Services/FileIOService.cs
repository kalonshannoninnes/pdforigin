using System.IO;
using System.Threading.Tasks;

namespace PDFOriginAvalonia.Services
{
    public class FileIoService : IFileIoService
    {
        public Task<string> GetOutputFileName(string inputFilePath, string outputDir, string fileNamePrefix)
        {
            var inputFileName = $"{fileNamePrefix}_<{Path.GetFileName(inputFilePath)}";
            return Task.FromResult(Path.Combine(outputDir, inputFileName));
        }
    }
}