using PDFOriginAvalonia.ViewModels;

namespace PDFOriginAvalonia.Models
{
    public class EmailConfig : ViewModelBase
    {
        public string Server { get; set; }
        public int Port { get; set; }

        public EmailConfig()
        {
            Server = "smtp.curtin.edu.au";
            Port = 25;
        }
    }
}