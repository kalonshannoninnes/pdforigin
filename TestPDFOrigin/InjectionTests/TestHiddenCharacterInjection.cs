﻿using System;
using System.IO;
using System.Text;
using Moq;
using NUnit.Framework;
using PDFOriginLib.Core;
using PDFOriginLib.Pdf;
using PDFOriginLib.Utils;
using PDFOriginLib.Utils.Encoding;

namespace TestPDFOrigin.InjectionTests
{
    [TestFixture]
    public class TestHiddenCharacterInjection : InjectionTestBase
    {
        private const string EncodedId = "id";
        private const char Delimiter = '-';
        
        private IEncoder _encoder;
        private IInjector _injector;

        [SetUp]
        public void Setup()
        {
            var encoderMock = new Mock<IEncoder>();
            encoderMock.Setup(e => e.Encode(It.IsAny<string>())).Returns(EncodedId);

            _encoder = encoderMock.Object;
            var pdfService = new iTextService(); // Need to mock this
            _injector = new PdfInjector(_encoder, pdfService);
        }

        [TearDown]
        public void Teardown()
        {
            _encoder = null;
            _injector = null;
        }
        
        [TestCase(0)]
        [TestCase(Id)]
        [TestCase(long.MaxValue)]
        public void Test_ApplyingCharacterFingerprint_IncreasesFileSizeByFactorOfEncodedIdLength(long id)
        {
            var originalPdfBytes = File.ReadAllBytes(PdfFile);

            var editedPdfBytes = _injector.InjectHiddenCharacters(originalPdfBytes, id);
            var diff = editedPdfBytes.Length - originalPdfBytes.Length;
            var diffIsFactorOfIdLength = (diff / Encoding.Default.GetBytes(EncodedId).Length) % 1 == 0;
            
            Assert.That(editedPdfBytes.Length, Is.GreaterThan(originalPdfBytes.Length));
            Assert.That(diffIsFactorOfIdLength, Is.True);
        }


        [TestCase(-1)]
        [TestCase(long.MinValue)]
        public void Test_NegativeId_ThrowsArgumentException(long insertedId)
        {
            var pdfBytes = File.ReadAllBytes(PdfFile);
            
            Func<byte[]> addingNegativeId = 
                () => _injector.InjectHiddenCharacters(pdfBytes, insertedId);
            
            Assert.That(addingNegativeId, Throws.InstanceOf<ArgumentException>());
        }

        [Test]
        public void Test_InvalidByteArray_ThrowsPdfException()
        {
            var randomBytes = new byte[1000];
            new Random().NextBytes(randomBytes);

            Func<byte[]> readingInvalidFile = 
                () => _injector.InjectHiddenCharacters(randomBytes, Id);
            
            Assert.That(readingInvalidFile, Throws.InstanceOf<PDFOriginException>());
        }
    }
}