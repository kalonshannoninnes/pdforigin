﻿using System;
using System.IO;
using NUnit.Framework;
using PDFOriginLib.Core;
using PDFOriginLib.Pdf;
using PDFOriginLib.Utils.Encoding;

namespace TestPDFOrigin.InjectionTests
{
    // TODO Figure out better way to test than filesize
    [TestFixture]
    public class TestWatermarkInjection : InjectionTestBase
    {
        private IInjector _injector;

        [SetUp]
        public void SetUp()
        {
            var encoder = new BinaryEncoder('a', 'z', '|');
            var pdfService = new iTextService();
            _injector = new PdfInjector(encoder, pdfService);
        }
        
        // itext doesn't necessarily use the same compression as the original PDF, so these tests are unreliable 
        
        // [TestCase(0)]
        // [TestCase(Id)]
        // [TestCase(286382)]
        // public void Test_ApplyingImageFingerprint_IncreasesFileSize(long id)
        // {
        //     var originalPdfBytes = File.ReadAllBytes(PdfFile);
        //     
        //     var editedPdfBytes = _injector.InjectWatermark(originalPdfBytes, id);
        //     
        //     Assert.That(editedPdfBytes.Length, Is.GreaterThan(originalPdfBytes.Length));
        // }
        
        [TestCase(-1)]
        [TestCase(long.MinValue)]
        public void Test_NegativeId_ThrowsArgumentException(long insertedId)
        {
            var pdfBytes = File.ReadAllBytes(PdfFile);
            
            Func<byte[]> addingNegativeId = () => _injector.InjectWatermark(pdfBytes, insertedId);
            
            Assert.That(addingNegativeId, Throws.InstanceOf<ArgumentException>());
        }

        [Test]
        public void Test_InvalidByteArray_ThrowsIOException()
        {
            var randomBytes = new byte[1000];
            new Random().NextBytes(randomBytes);

            Func<byte[]> readingInvalidFile = () => _injector.InjectWatermark(randomBytes, Id);
            
            Assert.That(readingInvalidFile, Throws.InstanceOf<PDFOriginException>());
        }
    }
}