﻿using System;
using Moq;
using NUnit.Framework;
using PDFOriginLib.Core;
using PDFOriginLib.Pdf;
using PDFOriginLib.Utils;
using PDFOriginLib.Utils.Encoding;

namespace TestPDFOrigin.ExtractionTests
{
    [TestFixture]
    public class TestHiddenCharacterExtraction
    {
        private const char Delimiter = '-';
        
        private IExtractor _extractor;
        
        [SetUp]
        public void SetUp()
        {
            var encoderMock = new Mock<IEncoder>();
            encoderMock.Setup(e => e.Decode(It.Is<string>(s => s.Contains(Delimiter)))).Returns("0");
            encoderMock.Setup(e => e.Decode(It.Is<string>(s => !s.Contains(Delimiter))))
                .Throws<FingerprintNotFoundException>();
            
            _extractor = new PdfExtractor(encoderMock.Object);
        }

        [TearDown]
        public void TearDown()
        {
            _extractor = null;
        }

        [TestCase("-fingerprint-Hello World", "fingerprint")]
        [TestCase("Hel-fingerprint-lo World", "fingerprint")]
        [TestCase("Hello-fingerprint- World", "fingerprint")]
        [TestCase("Hello World-fingerprint-", "fingerprint")]
        public void Test_HiddenCharExtractor_FindsFingerprintAnywhereInString(string fingerprintedText, string fingerprint)
        {
            var extractedFingerprint = _extractor.ExtractHiddenCharacters(fingerprintedText);

            Assert.That(extractedFingerprint, Is.EqualTo("0"));
        }

        [TestCase("Hello W=regular text=orld")]
        [TestCase("Hello World @regular text@")]
        public void Test_HiddenCharExtractor_ThrowsExceptionIfNoFingerprint(string plainText)
        {
            Func<string> extractingMissingFingerprint = 
                () => _extractor.ExtractHiddenCharacters(plainText);
            
            Assert.That(extractingMissingFingerprint, Throws.InstanceOf<FingerprintNotFoundException>());
        }

        [TestCase("He-fingerprint with spaces-llo World", "fingerprint with spaces")]
        [TestCase("Hello -fingerprint with spaces-World", "fingerprint with spaces")]
        public void Test_HiddenCharExtractor_FindsFingerprintWithWhitespace(string fingerprintedText, string fingerprint)
        {
            var extractedFingerprint = _extractor.ExtractHiddenCharacters(fingerprintedText);
            
            Assert.That(extractedFingerprint, Is.EqualTo("0"));
        }
    }
}