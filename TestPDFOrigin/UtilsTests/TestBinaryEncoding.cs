﻿using System;
using NUnit.Framework;
using PDFOriginLib.Utils;
using PDFOriginLib.Utils.Encoding;

namespace TestPDFOrigin.UtilsTests
{
    [TestFixture]
    public class TestBinaryEncoding
    {
        private IEncoder _encoder;

        private const char EncodedZero = 'a';
        private const char EncodedOne = 'z';
        
        [SetUp]
        public void SetUp()
        {
            _encoder = new BinaryEncoder(EncodedZero, EncodedOne, '|');
        }

        [TestCase("0", "|a|")]
        [TestCase("1", "|z|")]
        [TestCase("00110011", "|aazzaazz|")]
        public void Test_EncodingValidBinaryStrings_ReturnsEncoded(string toTest, string expected)
        {
            var encodedBinary = _encoder.Encode(toTest);

            Assert.That(encodedBinary, Is.EqualTo(expected));
        }

        [TestCase("a")]
        [TestCase("!@#$%^&*()-=+_`~")]
        [TestCase("1234567890.0987654321")]
        public void Test_EncodingInvalidStrings_ThrowsException(string toTest)
        {
            Func<string> attemptingToEncodeInvalidStrings = () => _encoder.Encode(toTest);
            
            Assert.That(attemptingToEncodeInvalidStrings, Throws.InstanceOf<EncodingException>());
        }

        [TestCase("||", "")]
        [TestCase("|aaaaa|", "00000")]
        [TestCase("|zzzzz|", "11111")]
        [TestCase("|azazazazaz|", "0101010101")]
        public void Test_DecodingValidEncodedString_ReturnsDecoded(string toTest, string expected)
        {
            var decodedBinary = _encoder.Decode(toTest);
            
            Assert.That(decodedBinary, Is.EqualTo(expected));
        }

        [TestCase("aaaaa")]
        [TestCase("zzzzz")]
        [TestCase("Hello World")]
        public void Test_DecodingInvalidStrings_ThrowsEncodingException(string toTest)
        {
            Func<string> attemptingToDecodeInvalidStrings = () => _encoder.Decode(toTest);
            
            Assert.That(attemptingToDecodeInvalidStrings, Throws.InstanceOf<EncodingException>());
        }
    }
}