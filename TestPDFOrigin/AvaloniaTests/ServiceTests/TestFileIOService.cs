using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using NUnit.Framework;
using PDFOriginAvalonia.Services;

namespace TestPDFOrigin.AvaloniaTests.ServiceTests
{
    [TestFixture]
    public class TestFileIOService
    {
        private IFileIoService _fileIoService;

        [SetUp]
        public void SetUp()
        {
            _fileIoService = new FileIoService();
        }

        [TearDown]
        public void TearDown()
        {
            _fileIoService = null;
        }

        [TestCase("/inputPath", "/outputPath", "prefix")]
        public async Task Test_GetOutputName_AddsPrefixToFilename(string inputPath, string outputDir, string prefix)
        {
            var outputPath = await _fileIoService.GetOutputFileName(inputPath, outputDir, prefix);
            var outputName = Path.GetFileName(outputPath);

            Assert.That(outputName, Does.StartWith(prefix));
        }

        // Work in progress
        [TestCase("/inputPath", "/outputPath", "prefix")]
        public async Task Test_GetOutputName_ReturnsValidPath(string inputPath, string outputDir, string prefix)
        {
            var outputPath = await _fileIoService.GetOutputFileName(inputPath, outputDir, prefix);
            var invalidChars = string.Join("", Path.GetInvalidPathChars());
            var numInvalidChars = outputPath.Intersect(invalidChars).Count();
            Console.WriteLine(string.Join("", invalidChars));
            Console.WriteLine(numInvalidChars);
            Console.WriteLine(outputPath);
            
            Assert.That(numInvalidChars, Is.Zero);
        }
    }
}